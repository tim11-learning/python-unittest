def is_polindrome(text: str, ignore_case: bool = False) -> bool:
    if ignore_case:
        return text.lower()[::-1] == text.lower()
    return text[::-1] == text


def reverse(s):
    if type(s) != str:
        raise TypeError('Expected str, got {}'.format(type(s)))

    return s[::-1]


def each2(text: str) -> str:
    return text[::2]