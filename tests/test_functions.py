import pytest
from src.functions import is_polindrome, reverse, each2


def test_is_polindrome():
    assert is_polindrome('abba')
    assert not is_polindrome('train')
    assert is_polindrome('Abba', True)


def test_reverse():
    assert reverse('blabla') == 'albalb'
    with pytest.raises(TypeError):
        reverse(12)


def test_each2():
    assert each2('abba') == 'ab'